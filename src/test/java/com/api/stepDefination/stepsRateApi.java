package com.api.stepDefination;

import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.junit.Assert;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
public class stepsRateApi {
	
	private static final String BASE_URL = "https://api.ratesapi.io/api";
	private static Response response;
	private static String jsonString;
	private static String dateParam;
	
	
	@When("User requests Get the latest details of rate api endpoint")
	public void user_requests_get_the_latest_details_of_rate_api_endpoint() {
		RestAssured.baseURI = BASE_URL;
		 RequestSpecification request = RestAssured.given();
		 response = request.get("/latest?symbols=USD,GBP");
		 System.out.println(response);
		 jsonString = response.asString();
		 System.out.println(jsonString);
		 Map<String, String> rate = JsonPath.from(jsonString).get("rates");
		 System.out.println(rate);
		 String rateparam = response.jsonPath().get("USD");
		 String dateParam = response.jsonPath().get("date");
		 System.out.println(rateparam);
		 System.out.println(dateParam);
		 Assert.assertTrue(rate.size() > 0);
		 
		  	      
	}

	@Then("User receives valid HTTP response code {int}")
	public void user_receives_valid_http_response_code(Integer int1) {
		Assert.assertEquals(200, response.getStatusCode());
		 
	    
	}



}
