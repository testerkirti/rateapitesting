package com.api.testRunner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/feature",
        glue = {"com.api.stepDefination"},
        monochrome = true,
        dryRun = false,
        plugin = {"pretty", "html:target/cucumber-reports"}
         
)
public class TestRunner {

}
